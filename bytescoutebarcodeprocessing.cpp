#include "bytescoutebarcodeprocessing.h"

#include <QDateTime>
#include <QDir>

#include <exception>
#include <stdio.h>

#include "atlbase.h"
#include <QDebug>
#include <set>
#include <iostream>

using namespace  Bytescout_BarCodeReader;

ByteScouteBarCodeProcessing::ByteScouteBarCodeProcessing(QObject *parent) : QObject(parent)
{
    if(!QDir("C:/cadr/CurrentWeek").exists())
        QDir().mkdir("C:/cadr/CurrentWeek");
    // Create the interface pointer.

    CoInitialize(NULL);

    pIReader = IReaderPtr(__uuidof(Reader));

    BSTR regname = ::SysAllocString(L"regname");
    pIReader->put_RegistrationName(regname);
    SysFreeString(regname);
    BSTR regkey = ::SysAllocString(L"regkey");
    pIReader->put_RegistrationKey(regkey);
    SysFreeString(regkey);

    _BarcodeTypeSelectorPtr pBarcodeTypesToFind;
    //    pBarcodeTypesToFind->put_QRCode(VARIANT_TRUE);
    pIReader->get_BarcodeTypesToFind(&pBarcodeTypesToFind);
    pBarcodeTypesToFind->put_DataMatrix(VARIANT_TRUE);
    pBarcodeTypesToFind->put_Code128(VARIANT_FALSE);
    pIReader->put_MaxNumberOfBarcodesPerPage(1);

//    pIReader->put_ColorConversionMode(ColorConversionMode_ImageBlocks);

//    std::cout << "ColorConversionMode_ImageBlocks\t\t";
    pIReader->put_DecodingTimeOut(2000);

    _ImagePreprocessingFiltersCollection* pIImageFilters;
    pIReader->get_ImagePreprocessingFilters(&pIImageFilters);
}
/*


ColorConversionMode
{
    ColorConversionMode_ImageBlocks = 0,
    ColorConversionMode_Image = 1,
    ColorConversionMode_Enhancing = 2,
    ColorConversionMode_NoiseFilter = 3,
    ColorConversionMode_Smoothed = 4,
    ColorConversionMode_GridFiltering = 5,
    ColorConversionMode_Threshold = 6,
    ColorConversionMode_Legacy = 7
};


*/
void ByteScouteBarCodeProcessing::ChangeConverMode(int a)
{
    switch (a)
    {
    case 0:
        pIReader->put_ColorConversionMode(ColorConversionMode_ImageBlocks);
        curConvMode = ColorConversionMode_ImageBlocks;
//        std::cout << "ColorConversionMode_ImageBlocks\t\t";
        break;
    case 1:
        pIReader->put_ColorConversionMode(ColorConversionMode_Enhancing);
        curConvMode = ColorConversionMode_Enhancing;
//        std::cout << "ColorConversionMode_Enhancing\t\t";
        break;
    case 2:
        pIReader->put_ColorConversionMode(ColorConversionMode_Image);
        curConvMode = ColorConversionMode_Image;
//        std::cout << "ColorConversionMode_Image\t\t";
        break;
    case 3:
        pIReader->put_ColorConversionMode(ColorConversionMode_Enhancing);
        curConvMode = ColorConversionMode_Enhancing;
//        std::cout << "ColorConversionMode_Enhancing\t\t";
        break;
    case 4:
        pIReader->put_ColorConversionMode(ColorConversionMode_Smoothed);
        curConvMode = ColorConversionMode_Smoothed;
//        std::cout << "ColorConversionMode_Smoothed\t\t";
        break;
    case 5:
        pIReader->put_ColorConversionMode(ColorConversionMode_GridFiltering);
        curConvMode = ColorConversionMode_GridFiltering;
//        std::cout << "ColorConversionMode_GridFiltering\t";
        break;
    case 6:
        pIReader->put_ColorConversionMode(ColorConversionMode_Threshold);
        curConvMode = ColorConversionMode_Threshold;
//        std::cout << "ColorConversionMode_Threshold\t\t";
        break;
    case 7:
        pIReader->put_ColorConversionMode(ColorConversionMode_Legacy);
        curConvMode = ColorConversionMode_Legacy;
//        std::cout << "ColorConversionMode_Legacy\t\t";
        break;
    default:
        break;
    }
}

ByteScouteBarCodeProcessing::~ByteScouteBarCodeProcessing()
{
    CoUninitialize();
}

int ByteScouteBarCodeProcessing::getLastGoodFrame() const
{
    return lastGoodFrame;
}

void ByteScouteBarCodeProcessing::setLastGoodFrame(int value)
{
    lastGoodFrame = value;
}
int ByteScouteBarCodeProcessing::getFrameToSave() const
{
    return frameToSave;
}

void ByteScouteBarCodeProcessing::setFrameToSave(int value)
{
    frameToSave = value;
}
QString ByteScouteBarCodeProcessing::getFolderName() const
{
    return FolderName;
}

void ByteScouteBarCodeProcessing::setFolderName(const QString &value)
{
    FolderName = value;
    if(!QDir("C:/cadr/CurrentWeek/"+FolderName).exists())
        QDir().mkdir("C:/cadr/CurrentWeek/"+FolderName);
}
bool ByteScouteBarCodeProcessing::getQrFound() const
{
    return qrFound;
}

void ByteScouteBarCodeProcessing::setQrFound(bool value)
{
    qrFound = value;
}

int ByteScouteBarCodeProcessing::getMainThresh() const
{
    return MainThresh;
}

void ByteScouteBarCodeProcessing::setMainThresh(int value)
{
    MainThresh = value;
}

void ByteScouteBarCodeProcessing::writemat()
{
    QString date = QDateTime::currentDateTime().toString("dd.MM.yyyy");

    if(!QDir("C:/cadr/CurrentWeek/"+FolderName+"/"+date).exists())
        QDir().mkdir("C:/cadr/CurrentWeek/"+FolderName+"/"+date);

    std::string date_of_incident = QDateTime::currentDateTime().toString("dd_MM hh_mm_ss").toStdString();

    QDir().mkdir("C:/cadr/CurrentWeek/"+FolderName+"/"+date+"/"+QDateTime::currentDateTime().toString("dd_MM hh_mm_ss"));

    for (int i=0;i<mv.size();i++)
    {
        cv::Mat wrmat = mv[i];
        // cv::resize(wrmat,wrmat,cv::Size(640,480)); // REview
        cv::String img_file_name = "C:/cadr/CurrentWeek/"+FolderName.toStdString()+"/"+date.toStdString()+"/"+date_of_incident+"/"+QString::number(i).toStdString()+".jpg";
        cv::imwrite(img_file_name,wrmat);
    }
    mv.clear();
}

void ByteScouteBarCodeProcessing::UpdateDB()
{
    QDir dir("C:/cadr/PreviousWeek");
    dir.removeRecursively();

    QDir dir1("C:/cadr");
    dir1.rename("CurrentWeek","PreviousWeek");

    if(!QDir("C:/cadr/CurrentWeek").exists())
    {
        QDir().mkdir("C:/cadr/CurrentWeek");
        QDir().mkdir("C:/cadr/CurrentWeek/cam1");
        QDir().mkdir("C:/cadr/CurrentWeek/cam2");
    }

    currentweek = QDate::currentDate().weekNumber();
}

void ByteScouteBarCodeProcessing::ClearBoxCodes()
{

}

const QStringList ByteScouteBarCodeProcessing::HandlePDF(const QString &FileName)
{

    //qDebug()<<"ProcessFrame";
    QString workFolder = "C:/tmp/";
    QString pathFilePDF = workFolder + FileName;
    HRESULT hr = CoInitialize(NULL);
    WCHAR file[MAX_PATH];
    ::GetFullPathName(pathFilePDF.toStdWString().c_str(), MAX_PATH, file, NULL);
    // Read barcode from file
    long temp;
    hr = pIReader->ReadFromPdfFile(_bstr_t(file), &temp);
    if (hr == E_FAIL)
    {
        wprintf(L"File was not found: %s\n", file);
    }
    QStringList codes;
    // Get full path of sample barcode image file
    // Get found barcode count
    long count;
    pIReader->get_FoundCount(&count);

    // Get found barcode information
    for (int i = 0; i < count; i++)
    {
        //qDebug() << this << "step " << i << "is " << count;
        SymbologyType type;
        hr = pIReader->GetFoundBarcodeType(i, &type);

        float confidence;
        hr = pIReader->GetFoundBarcodeConfidence(i, &confidence);

        BSTR bstrValue;
        hr = pIReader->GetFoundBarcodeValue(i, &bstrValue);

        long t;
        hr = pIReader->GetFoundBarcodeTop(i,&t);

        long l;
        hr = pIReader->GetFoundBarcodeLeft(i,&l);

        long w;
        hr = pIReader->GetFoundBarcodeWidth(i,&w);

        long h;
        hr = pIReader->GetFoundBarcodeHeight(i,&h);
        QString res = QString::fromWCharArray(bstrValue);
        for(int i = 0; i < 10; ++i)
        {
            if(res.at(0) != '0')
            {
                res = res.right(res.size() - 1);
            }
        }
        res.remove("<FNC1>");
        codes.append(res);
    }
    // Uninitialize COM.
    CoUninitialize();
    return codes;
    //     saveCSVFile();
}


bool compareContourAreas (std::vector<cv::Point> contour1, std::vector<cv::Point> contour2 )
{
    double i = fabs( contourArea(cv::Mat(contour1)) );
    double j = fabs( contourArea(cv::Mat(contour2)) );
    return ( i < j );
}

struct ResultWithImage
{

    std::set<QString> Data;

    std::shared_ptr<cv::Mat> Image = nullptr;

    int ID{ 0 };

    bool bForsed{ false };

};
std::wstring s2ws(const std::string& str)
{
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo( size_needed, 0 );
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}
Stats ByteScouteBarCodeProcessing::HandleFrame(QString fileName)
{
    QDateTime dbg = QDateTime::currentDateTime();
    //qDebug() << "start" << dbg;
    ResultWithImage *Result = new ResultWithImage();
    Stats ret;
    ret.CurrentMode = curConvMode;
    //    cv::Mat d;
    //    d = cv::imread("C:/tmp/2.png");

    //    cv::imshow("", d);

    //    //qDebug() << "Draw Framre" << dbg;
    ////    cv::Mat draw = frame->clone();

    //    QString workFolder = "C:/tmp";
    //qDebug() << this << "Start save mat" << QDateTime::currentMSecsSinceEpoch();
    //qDebug() << this << "Complete save mat" << QDateTime::currentMSecsSinceEpoch();


    //    cvtColor(d, d, cv::COLOR_GRAY2RGB); // REview
    //    cv::imwrite(workFolder.toStdString()+"/1.png",d);
    //    QString filename = workFolder + "1.png";

    WCHAR file[MAX_PATH];

    HRESULT hr;


    ::GetFullPathName(s2ws(fileName.toStdString()).data(), MAX_PATH, file, NULL);

    //qDebug() << this << "Start read file" << QDateTime::currentMSecsSinceEpoch();
    // Read barcode from file
    hr = pIReader->ReadFromFile(_bstr_t(file));

    /*

typedef struct tagSAFEARRAY
    {
    USHORT cDims;
    USHORT fFeatures;
    ULONG cbElements;
    ULONG cLocks;
    PVOID pvData;
    SAFEARRAYBOUND rgsabound[ 1 ];
    } 	SAFEARRAY;

*/
    //    hr = pIReader->ReadFromMemory(reinterpret_cast<SAFEARRAY*>(d.data));
    // Get full path of sample barcode image file

    //qDebug() << this << "Complete read file" << QDateTime::currentMSecsSinceEpoch();
    // Get found barcode count
    long count;
    pIReader->get_FoundCount(&count);

    //    cv::cvtColor(*frame.second, *frame.second, cv::COLOR_BGR2BGRA);// REview
    //qDebug() << this << "Code count" << count << hr;
    QStringList listCode;
    // Get found barcode information
    //qDebug() << this << "Start handle codes" << QDateTime::currentMSecsSinceEpoch();
    for (int i = 0; i < count; i++)
    {
        SymbologyType type;
        hr = pIReader->GetFoundBarcodeType(i, &type);

        float confidence;
        hr = pIReader->GetFoundBarcodeConfidence(i, &confidence);

        if(confidence>0)
            correctCode = 1;
        else
            correctCode = 2;


        BSTR bstrValue;
        hr = pIReader->GetFoundBarcodeValue(i, &bstrValue);

        long t;
        hr = pIReader->GetFoundBarcodeTop(i,&t);

        long l;
        hr = pIReader->GetFoundBarcodeLeft(i,&l);

        long w;
        hr = pIReader->GetFoundBarcodeWidth(i,&w);

        long h;
        hr = pIReader->GetFoundBarcodeHeight(i,&h);

        QString res = QString::fromWCharArray(bstrValue);
        res.remove("<FNC1>");

        //        cv::Rect rec = cv::Rect(l,t,w,h);
        //qDebug()<<"res="<<res;
        qrFound = true;
        readCodes.append(res);// REview
        //        cv::rectangle(*frame.second,rec,cv::Scalar(0,255,0),10);
        Result->Data.insert(res);
    }

    emit NextFrame();
    emit ProcessingRequest(true);
    //emit ElapsedTime(frameToSave);
//    qDebug() << "end" << dbg.msecsTo(QDateTime::currentDateTime()) << readCodes;
    ret.EncodngTime = dbg.msecsTo(QDateTime::currentDateTime());
    ret.HaveCode = !readCodes.empty();
    readCodes.clear();// REview
    return ret;
}
