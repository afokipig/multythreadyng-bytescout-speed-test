#ifndef BYTESCOUTEBARCODEPROCESSING_H
#define BYTESCOUTEBARCODEPROCESSING_H

#include <QObject>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <opencv2/objdetect.hpp>
#include "bytescout.barcodereader.tlh"

struct Stats
{
    Bytescout_BarCodeReader::ColorConversionMode CurrentMode;
    int EncodngTime;
    bool HaveCode;
};

class ByteScouteBarCodeProcessing : public QObject
{
    Q_OBJECT
public:
    explicit ByteScouteBarCodeProcessing(QObject *parent = nullptr);
    ~ByteScouteBarCodeProcessing();
    std::vector<cv::Mat> mv;
    double alph=0;
    int addthres=0;
    int getLastGoodFrame() const;
    void setLastGoodFrame(int value);

    int getFrameToSave() const;
    void setFrameToSave(int value);

    QString getFolderName() const;
    void setFolderName(const QString &value);

    bool getQrFound() const;
    void setQrFound(bool value);
    int currentweek=0;
    int correctCode =0;
    Bytescout_BarCodeReader::ColorConversionMode curConvMode;
    int curMode = 0;
    void ChangeConverMode(int a);
private:
    int MainThresh;
    int lastGoodFrame=0;
    int frameToSave=15;
    Bytescout_BarCodeReader::IReaderPtr pIReader;


    cv::Mat m;
    cv::Mat mToSave;
    int nam=1;

    QString FolderName;
    QStringList readCodes; // Р СљР В°РЎРѓРЎРѓР С‘Р Р† Р С—РЎР‚Р С•РЎвЂЎР С‘РЎвЂљР В°Р Р…Р Р…РЎвЂ№РЎвЂ¦ Р С”Р С•Р Т‘Р С•Р Р†
    QStringList sameCodes;

    bool qrFound = false;
    QString LastRes="null";
public slots:
    Stats HandleFrame(QString fileName) ;
    int getMainThresh() const;
    void setMainThresh(int value);
    void writemat();
    void UpdateDB();
    void ClearBoxCodes();
    const QStringList HandlePDF(const QString &FileName);

signals:
     void NextFrame();
     void ProcessingRequest(bool request);
     void CodeDetected(bool isFind);
     void ElapsedTime(int msec);
     void Code(QString code);
     void CodeQr(QString code);
     void SearchResult(QString res);
     void BarCodeROI(cv::Rect BC_ROI);
     void ThreshMat(cv::Mat);

     // CodeProcessing interface
};

#endif // BYTESCOUTEBARCODEPROCESSING_H
