#include <QCoreApplication>

#include "bytescoutebarcodeprocessing.h"
#include <QDir>
#include <QFile>
#include <QDebug>

#include <QDirIterator>

#include <vector>
#include <iostream>
#include <thread>
#include <mutex>

std::mutex globalMtx;

QStringList getPathFiles(QString pathFolder)
{
    QStringList pathFiles;
    QDirIterator ItR(pathFolder, {"*.jpg", "*.png"}, QDir::Files, QDirIterator::Subdirectories);
    while (ItR.hasNext())
    {
        pathFiles.append(ItR.next());
    }

    return pathFiles;
}

struct Regim
{
    int readCount{0};
    int minTime{2000};
    int maxTime{0};
    float avgTime{0.f};
    std::vector<int> readedTimes;
    std::vector<int> unreadedTimes;
    void AddStat(const Stats& stat)
    {
        if(stat.HaveCode)
        {
            ++readCount;
            readedTimes.push_back(stat.EncodngTime);
            if(stat.EncodngTime < minTime)
            {
                minTime = stat.EncodngTime;
            }
            if(stat.EncodngTime > maxTime)
            {
                maxTime = stat.EncodngTime;
            }
        }
        else
        {
            unreadedTimes.push_back(stat.EncodngTime);
        }
    }

    void CalculateStats()
    {
        long double totalTime = 0;
        for(int val : readedTimes)
        {
            totalTime += val;
        }
        avgTime = totalTime / readedTimes.size();
    }


};

struct GlobalStats
{
    std::mutex statMtx;
    QMap<Bytescout_BarCodeReader::ColorConversionMode,Regim> tableStatic =
    {
        {Bytescout_BarCodeReader::ColorConversionMode_ImageBlocks, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_Image, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_Enhancing, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_NoiseFilter, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_Smoothed, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_GridFiltering, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_Threshold, {}},
        {Bytescout_BarCodeReader::ColorConversionMode_Legacy, {}}
    };
    void addStat(const Stats& stat)
    {
        statMtx.lock();
        tableStatic.find(stat.CurrentMode).value().AddStat(stat);
        statMtx.unlock();
    }
    void CalculateStats()
    {
        for(auto &item : tableStatic)
        {
            item.CalculateStats();
        }
    }

    void ShowStats(Bytescout_BarCodeReader::ColorConversionMode mode)
    {
        std::cout << "********************************************************************************" << std::endl;
        switch(mode)
        {
        case Bytescout_BarCodeReader::ColorConversionMode_ImageBlocks:
            std::cout << "ColorConversionMode_ImageBlocks\t\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_Image:
            std::cout << "ColorConversionMode_Image\t\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_Enhancing:
            std::cout << "ColorConversionMode_Enhancing\t\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_NoiseFilter:
            std::cout << "ColorConversionMode_NoiseFilter\t\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_Smoothed:
            std::cout << "ColorConversionMode_Smoothed\t\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_GridFiltering:
            std::cout << "ColorConversionMode_GridFiltering\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_Threshold:
            std::cout << "ColorConversionMode_Threshold\t\t" << std::endl;
            break;
        case Bytescout_BarCodeReader::ColorConversionMode_Legacy:
            std::cout << "ColorConversionMode_Legacy\t\t" << std::endl;
            break;

        }
        std::cout << "ReadCount " << tableStatic.value(mode).readCount << std::endl;
        std::cout << "MaxTime " << tableStatic.value(mode).maxTime << std::endl;
        std::cout << "MinTime " << tableStatic.value(mode).minTime << std::endl;
        std::cout << "avgTime " << tableStatic.value(mode).avgTime << std::endl;
    }
    void showAllstat()
    {
        for(const auto mode : tableStatic.keys())
        {
            ShowStats(mode);
        }
    }
};

static std::mutex arrayMtx;

QString GetLast(QStringList* list)
{
    QString item;
    arrayMtx.lock();
    if(list->empty())
    {
        arrayMtx.unlock();
        return item;
    }
    item = list->last();
    list->pop_back();
    arrayMtx.unlock();
    globalMtx.lock();
    system("cls");
    std::cout << "to decode "<< list->size() << std::endl;
    globalMtx.unlock();

    return item;
}

void StartMultyThreading(GlobalStats* gstat, QStringList* list)
{
    ByteScouteBarCodeProcessing bar;

    QString item{ GetLast(list) };

    while(!item.isEmpty())
    {
        for(int i = 0; i < 8; i++)
        {
            bar.ChangeConverMode(i);
            gstat->addStat(bar.HandleFrame(item));
        }
        item = GetLast(list);
    }

}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString Path{"C:/tmp"};

    QStringList files = getPathFiles(Path);

    ByteScouteBarCodeProcessing bar;

    GlobalStats gstat;

    std::thread th1{StartMultyThreading, &gstat, &files };
    std::thread th2{StartMultyThreading, &gstat, &files };
    std::thread th3{StartMultyThreading, &gstat, &files };
    std::thread th4{StartMultyThreading, &gstat, &files };
    std::thread th5{StartMultyThreading, &gstat, &files };
    std::thread th6{StartMultyThreading, &gstat, &files };
    std::thread th7{StartMultyThreading, &gstat, &files };

    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();
    th6.join();
    th7.join();

    system("cls");
    gstat.CalculateStats();
    gstat.showAllstat();


    return a.exec();
}
