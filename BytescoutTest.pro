QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        bytescoutebarcodeprocessing.cpp \
        main.cpp








# Include OpenCV
INCLUDEPATH += C:\opencv\include
LIBS += -LC:\opencv\lib\Release\
        -lopencv_core430 \
        -lopencv_highgui430 \
        -lopencv_imgcodecs430 \
        -lopencv_imgproc430 \
        -lopencv_photo430

LIBS += -LC:\opencv\lib\Debug\
        -lopencv_core430d \
        -lopencv_highgui430d \
        -lopencv_imgcodecs430d \
        -lopencv_imgproc430d \
        -lopencv_photo430d



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    bytescout.barcodereader.tlh \
    bytescoutebarcodeprocessing.h
